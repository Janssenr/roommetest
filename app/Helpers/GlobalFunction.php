<?php

namespace App\Helpers;

use App\User;
use Illuminate\Support\Str;

class GlobalFunction
{
    public static function GenerateToken(){
        $result = Str::random(60);

        $isExist = User::where('token',$result)->exists();

		if($isExist){
            $result = GlobalFunction::GenerateToken();
        }
        
		return $result;
    }
}