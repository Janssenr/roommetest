<?php

namespace App\Http\Controllers;

use App\User;
use App\Helpers\GlobalFunction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class ApiController extends Controller
{
    public function authenticate($token)
    {
        return User::where('token', $token)->first();
    }

    public function login(Request $request)
    {
        $result = [];
        $result['code'] = 403;
        $result['message'] = "error";
        $result['data'] = 'Credential not match';

        $email = $request->email;
        $password = $request->password;

        $user = User::where('email', $email)->first();

        if($user) {
            if(Hash::check($password, $user->password)) {
                $token = GlobalFunction::GenerateToken();
                User::where('id', $user->id)->update(['token' => $token]);
    
                $result['code'] = 200;
                $result['message'] = "success";
                $result['data'] = User::where('id', $user->id)->first();
            }
        }

        return compact('result');
    }

    public function createUser(Request $request)
    {
        $auth = $this->authenticate($request->bearerToken());

        $result = [];
        $result['code'] = 403;
        $result['message'] = 'error';
        $result['data'] = 'Create User Failed';

        if($auth) {
            $data = new User();
            $data->name = $request->name;
            $data->email = $request->email;
            $data->privilege = $request->privilege;
            $data->password = Hash::make($request->password);
            $data->created_by = $auth->id;
            $data->save();

            $result['code'] = 200;
            $result['message'] = 'success';
            $result['data'] = 'Create User Success';
        }

        return compact('result');
    }

    public function updateUser(Request $request)
    {
        $auth = $this->authenticate($request->bearerToken());

        $result = [];
        $result['code'] = 403;
        $result['message'] = 'error';
        $result['data'] = 'Update User Failed';

        if($auth) {
            $user = User::where('id',$request->user_id)->first();
            $data = [];
            $data['name'] = $request->name;
            if($user->email != $request->email) {
                $data['email'] = $request->email;
            }
            $data['privilege'] = $request->privilege;
            $data['password'] = Hash::make($request->password);
            $data['updated_by'] = $auth->id;

            $updated = User::where('id',$request->user_id)->update($data);

            if($updated) {
                $result['code'] = 200;
                $result['message'] = 'success';
                $result['data'] = 'Update User Success';
            }
        }

        return compact('result');
    }

    public function deleteUser(Request $request)
    {
        $auth = $this->authenticate($request->bearerToken());

        $result = [];
        $result['code'] = 403;
        $result['message'] = 'error';
        $result['data'] = 'Delete User Failed';

        if($auth) {
            $user = User::where('id',$request->user_id)->first();
            User::where('id',$user->id)->update(['deleted_by' => $auth->id]);

            $deleted = User::where('id',$user->id)->delete();

            if($deleted) {
                $result['code'] = 200;
                $result['message'] = 'success';
                $result['data'] = 'Delete User Success';
            }
        }

        return compact('result');
    }

    public function checkPrivileges(Request $request)
    {
        $auth = $this->authenticate($request->bearerToken());

        $result = [];
        $result['code'] = 403;
        $result['message'] = 'error';
        $result['data'] = 'Does not have privileges';

        if($auth) {
            if($auth->privilege == $request->menu) {
                $result['code'] = 200;
                $result['message'] = 'success';
                $result['data'] = 'Have privileges';
            }
        }

        return compact('result');
    }
}
