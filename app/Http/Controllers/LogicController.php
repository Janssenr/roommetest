<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LogicController extends Controller
{
    //1. Remove String Spaces
    public function no_space(Request $request)
    {
        $result = str_replace(' ','',$request->s);

        return compact('result');
    }

    //2. Sum Mixed Array
    public function sum_mix(Request $request)
    {
        $result = array_sum($request->a);

        return compact('result');
    }

    //3. Sort and Star
    public function twoSort(Request $request)
    {
        $s = $request->s;
        $result = '';

        sort($s);

        $first = $s[0];
        $first_split = str_split($first);

        for($i = 0; $i < strlen($first); $i++) {
            if($i == 0) {
                $result = $first_split[$i];
            } else {
                $result = $result."***".$first_split[$i];
            }
        }

        return compact('result');
    }

    //4. MaShortest Word
    public function findShort(Request $request)
    {
        $str = $request->str;
        $shortest = 0;

        $words = explode(" ",$str);

        foreach($words as $item) {
            if($shortest == 0) {
                $shortest = strlen($item);
            }

            if(strlen($item) <= $shortest) {
                $shortest = strlen($item);
            }
        }

        $result = $shortest;

        return compact('result');
    }

    //5. Remove anchor from URL
    public function replaceAll(Request $request)
    {
        $string = $request->string;

        $url = explode("#",$string);

        $result = $url[0];

        return compact('result');
    }

    //6. Get the Middle Character
    public function getMiddle(Request $request)
    {
        $text = $request->text;
        $position = 0;
        $length = 0;

        if(strlen($text) % 2 == 0) {
            $position = strlen($text)/2-1;
            $length = 2;
        } else {
            $position = floor(strlen($text)/2);
            $length = 1;
        }

        $result = substr($text, $position, $length);

        return compact('result');
    }

    //7. Counting Duplicates
    public function duplicateCount(Request $request)
    {
        $text = $request->text;
        $string = strtolower($text);
        $total = 0;

        foreach(count_chars($string, 1) as $i => $val) {
            if($val > 1) {
                $total += 1;
            }
        }

        $result = $total;

        return compact('result');
    }

    //8. Are the the "same"?
    public function comp(Request $request)
    {
        $a = $request->a;
        $b = $request->b;
        $result = true;

        if(count($a) != count($b) || $a ==  null || $b == null){
            $result = false;
        } else {
            sort($a);
            sort($b);

            for($i = 0; $i < count($a); $i++) {
                if($a[$i] * $a[$i] != $b[$i]) {
                    $result = false;
                    break;
                }
            }
        }

        return compact('result');
    }

}
