<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'ApiController@login');
Route::post('create', 'ApiController@createUser');
Route::post('update', 'ApiController@updateUser');
Route::post('delete', 'ApiController@deleteUser');
Route::post('privilege', 'ApiController@checkPrivileges');
//Logic Test
Route::prefix('logic')->group(function () {
    Route::post('no-space', 'LogicController@no_space');
    Route::post('sum-mix', 'LogicController@sum_mix');
    Route::post('two-sort', 'LogicController@twoSort');
    Route::post('find-short', 'LogicController@findShort');
    Route::post('replace-all', 'LogicController@replaceAll');
    Route::post('get-middle', 'LogicController@getMiddle');
    Route::post('duplicate-count', 'LogicController@duplicateCount');
    Route::post('compare', 'LogicController@comp');
});